# projet_agence_logo

## lancer le projet
```
git clone git@gitlab.com:malandel/projet_agence_logo.git
cd projet_agence_logo
npm install
//lancer parcel
npm run start

```
## adresse en ligne

http://agence-logo.surge.sh

## mettre en ligne avec surge

install surge :

```
npm run surge

```
publier le site à l'adresse mon-adresse.surge.sh :

```
cd dossier-projet/dist/
surge --domain https://mon-adresse.surge.sh

```

## Kanban Board 

https://trello.com/b/HhbVZB4o/sitelogo



